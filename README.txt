
This module will make your thumbnails square.


Why
---
Maybe it's just my OCD (http://en.wikipedia.org/wiki/OCD), but I find the randomness of the standard thumbnails really nasty. This module allows you to have a much smoother flow to your gallery pages.


How
---
It works by finding the largest square that will fit into the original image, centering it, cropping, then scaling down. This doesn't effect the original image.

You will lose a small amount from the top & bottom or left & right in the thumbnail, but you will not get any image distortion. Most images look fine with this cropping.


Options
-------
There are no options for this module. If you enable it on your site it will make your thumbnails square. The only variable is the size of the thumbnails, which uses the 'width' setting for thumbnails (found in the Image administration page: http://yourDrupalSite/?q=admin/settings/image).


When you enable this module on your site existing thumbnails will NOT be regenerated automatically. To make this happen you need to force a regeneration. I found that changing the thumbnail dimensions triggered this, but there may well be a better way. Just changing the height value is good because that is not used by this module. Then when a thumbnail is required on a page it will be regenerated (as a square). A message will appear at the top of your page to say that the thumbnail has been regenerated, but this only happens once, it's standard behaviour of the image module.


Future development
------------------

Maybe this module should have it's own admin setting to specify the thumbnail size. I wasn't that bothered so i didn't do it. If someone else feels that it should then please feel free to make that change.
